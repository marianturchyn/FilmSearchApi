CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `year` int(10) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `imageUrl` varchar(255) NOT NULL,
  `imdbId` varchar(255) NOT NULL,
  `imdbRating` int(10) NOT NULL,
  `duration` int(10) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;