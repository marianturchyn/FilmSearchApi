var http = require('http');
var express = require('express');
var app = express();
var mysql      = require('mysql');
var bodyParser = require('body-parser');
var cors = require('cors');

//start mysql connection
// var connection = mysql.createConnection({
//   host     : 'localhost', //mysql database host name
//   user     : 'root', //mysql database user name
//   password : '', //mysql database password
//   database : 'test' //mysql database name
// });

var db_config = {
    host     : 'us-cdbr-iron-east-01.cleardb.net', //mysql database host name
    user     : 'b4d6d0bb43fcb9', //mysql database user name
    password : '5c50a5d6', //mysql database password
    database : 'heroku_ad710ab19790b47' //mysql database name
};
var connection; // = mysql.createConnection(db_config);

// connection.connect(function(err) {
//   if (err) // throw err;
//   console.log('Error, database...', err);
//
//   console.log('You are now connected with mysql database...');
// });
//end mysql connection

function handleDisconnect() {
    connection = mysql.createConnection(db_config); // Recreate the connection, since
                                                    // the old one cannot be reused.

    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

handleDisconnect();

//start body-parser configuration
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
//end body-parser configuration

//create app server
var port = process.env.PORT || 3000;

var server = app.listen(port, '0.0.0.0', () => {

  var host = server.address().address;

  console.log('Example app listening at http://%s:%s', host, port)

});

const whitelist = [
    'http://localhost:4200',
    'https://www.ufilms.net',
    'http://www.ufilms.net',
    'https://ufilms.net',
    'http://ufilms.net',
    'https://ufilms-client.herokuapp.com'
];
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    }
};

app.use(cors(corsOptions));

app.get('/*', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    /*res.header('X-XSS-Protection' , 0);

    const allowedOrigins = ['https://www.ufilms.net', 'http://localhost:4200', 'https://ufilms-client.herokuapp.com'];
    const origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    }

    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
*/
    next();
});

app.get('/ok', function (req, res, next) {
    res.end(JSON.stringify({ok: true}));
});

//rest api to get all films
app.get('/films', function (req, res) {
    const limit = +(req.query.limit || 10);
    const offset = +(req.query.offset || 0);
    const sort = req.query.sort;

    var categories = req.query.categories;
    var categoriesWhere = '';

    if (categories) {
        categories = Array.isArray(categories) ? categories : [categories];

        categoriesWhere = `WHERE categories LIKE '%${categories[0]}%'`;

        for (var i = 1; i < categories.length; i++) {
            categoriesWhere += ` AND categories LIKE '%${categories[i]}%'`;
        }
    }

    const categoriesSort = (sort === 'saturation') ?
        `ORDER BY CHAR_LENGTH(categories) ASC, categories DESC` :
        `ORDER BY imdbRating DESC`;

    const query = mysql.format(`SELECT * FROM film ${categoriesWhere} ${categoriesSort} LIMIT ? OFFSET ?`, [limit, offset]);

    console.log('sql:', query);

    connection.query(query, function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});

// select * from film
// where categories like '%Drama%' order by CHAR_LENGTH(categories) ASC, categories DESC

app.get('/film/:hash', function (req, res) {
    const hash = req.params.hash;

    const query = mysql.format(`SELECT * FROM film WHERE hash = ?`, [hash]);

    connection.query(query, function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});

// categories tree
app.get('/categories-tree', function (req, res) {
    connection.query('SELECT DISTINCT categories FROM film ORDER BY CHAR_LENGTH(categories) DESC, categories ASC', function (error, results, fields) {
        if (error) throw error;

        const data = results.map((row) => row.categories);

        res.end(JSON.stringify(data));
    });
});

// lists
// get list
app.get('/lists', function (req, res) {
    connection.query('SELECT * FROM list', function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});

// get films for list
app.get('/films-for-list/:id', function (req, res) {
    const listId = req.params.id;

    connection.query(`
    select * from film_list 
        left join list on list.id = film_list.listId
        left join film on film.id = film_list.filmId
        where listId=?
    `, [listId], function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});

// create list
app.post('/lists', function (req, res) {
    var params  = req.body;
    const title = params.title;

    connection.query('INSERT INTO list SET `title`=?', [title], function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify({
            id: results.insertId,
            title: title
        }));
    });
});

// update list title
app.put('/lists', function (req, res) {
    var params  = req.body;
    const id = params.id;
    const title = params.title;

    connection.query('UPDATE list SET `title`=? WHERE `id`=?', [title, id], function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify({
            id: id,
            title: title
        }));
    });
});

// add film to list
app.post('/add-film-to-list', function (req, res) {
    var params  = req.body;
    const listId = params.listId;
    const filmId = params.filmId;

    connection.query('INSERT INTO film_list SET `listId`=?, `filmId`=?', [listId, filmId], function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify({
            id: results.insertId,
            listId: listId,
            filmId: filmId
        }));
    });
});

// SELECT DISTINCT categories FROM film
//  ORDER BY CHAR_LENGTH(categories) DESC, categories ASC

/*//rest api to get all customers
app.get('/customer', function (req, res) {
   connection.query('select * from customer', function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});
//rest api to get a single customer data
app.get('/customer/:id', function (req, res) {
   connection.query('select * from customers where Id=?', [req.params.id], function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});

//rest api to create a new customer record into mysql database
app.post('/customer', function (req, res) {
   var params  = req.body;
   console.log(params);
   connection.query('INSERT INTO customer SET ?', params, function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});

//rest api to update record into mysql database
app.put('/customer', function (req, res) {
   connection.query('UPDATE `customer` SET `Name`=?,`Address`=?,`Country`=?,`Phone`=? where `Id`=?', [req.body.Name,req.body.Address, req.body.Country, req.body.Phone, req.body.Id], function (error, results, fields) {
	  if (error) throw error;
	  res.end(JSON.stringify(results));
	});
});

//rest api to delete record from mysql database
app.delete('/customer', function (req, res) {
   console.log(req.body);
   connection.query('DELETE FROM `customer` WHERE `Id`=?', [req.body.Id], function (error, results, fields) {
	  if (error) throw error;
	  res.end('Record has been delete!');
	});
});*/

// Handle errors
app.use((err, req, res, next) => {
    if (! err) {
        return next();
    }

    res.status(500);
    res.send('500: Internal server error');
});

function errorHandler (err, req, res, next) {
    res.status(500);
    res.send('500: Internal server error');
}
/*
Action
Adventure
Animation
Biography
Comedy
Crime
Documentary
Drama
Family
Fantasy
History
Horror
Music
Musical
Mystery
News
Reality-TV
Romance
Sci-Fi
Sport
Thriller
Uncategorized
War
Western
*/